using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private AudioClip enemyFireSound;
        [SerializeField] private float enemyFireSoundVolume = 0.1f;
        [SerializeField] private AudioClip enemyExplodeSound;
        [SerializeField] private float enemyExpoldeSoundVolume = 0.1f;
        
        [SerializeField] public double fireRate = 1f;
        
        private float fireCounter = 0;

        public void Init(int maxHp, float speed)
        {
            base.Init(maxHp, speed, defaultBullet);
        }
        
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            AudioSource.PlayClipAtPoint(enemyExplodeSound, Camera.main.transform.position, enemyExpoldeSoundVolume);
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                AudioSource.PlayClipAtPoint(enemyFireSound, Camera.main.transform.position, enemyFireSoundVolume);
                var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
        }
    }
}