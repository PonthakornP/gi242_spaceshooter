using UnityEngine;

namespace Spaceship
{
    public abstract class Basespaceship : MonoBehaviour
    {
        [SerializeField] protected Bullet defaultBullet;
        [SerializeField] protected Transform gunPosition;
        
        public int Hp { get; protected set; }
        public int MaxHp { get; private set; }
        public float Speed { get; private set; }
        public Bullet Bullet { get; private set; }

        protected void Init(int maxHp, float speed, Bullet bullet)
        {
            Hp = maxHp;
            MaxHp = maxHp;
            Speed = speed;
            Bullet = bullet;
        }

        public abstract void Fire();
    }
}