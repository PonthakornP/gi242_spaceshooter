using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [SerializeField] private AudioClip playerFireSound;
        [SerializeField] private float playerFireSoundVolume = 0.3f;
        [SerializeField] private AudioClip playerExplodeSound;
        [SerializeField] private float playerExplodeSoundVolume = 0.1f;
        
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(playerFireSound != null, "playerFireSound cannot be null");
            Debug.Assert(playerExplodeSound != null, "playerExplodeSound cannot be null");
        }

        private void Update()
        {
            print(Hp);
        }

        public void Init(int maxHp, float speed)
        {
            base.Init(maxHp, speed, defaultBullet);
        }

        public void GetHealUp(int hp)
        {
            Hp += hp;
            
            if (Hp > MaxHp)
            {
                Hp = MaxHp;
            }
            
            UIManager.Instance.SetHealth(Hp);
        }

        public void GetPowerUpBullet(Bullet bullet)
        {
            defaultBullet = bullet;
        }

        public override void Fire()
        {
            AudioSource.PlayClipAtPoint(playerFireSound, Camera.main.transform.position, playerFireSoundVolume);
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            UIManager.Instance.SetHealth(Hp);
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            Debug.Assert(Hp <= 0, "HP is more than zero");
            AudioSource.PlayClipAtPoint(playerExplodeSound, Camera.main.transform.position, playerExplodeSoundVolume);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}