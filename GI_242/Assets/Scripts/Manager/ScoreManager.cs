﻿using System;
using TMPro;
using UnityEngine;
using Utilities;

namespace Manager
{
    public class ScoreManager : MonoSingleton<ScoreManager>
    {
        
        public int Score { get; private set; }

        public event Action ScoreUpdated;

        public void Init()
        {
            Reset();
        }

        public void SetScore(int score)
        {
            Score += score;
            UIManager.Instance.SetScoreText($"Score : {Score}");
            
            ScoreUpdated?.Invoke();
        }

        public void Reset()
        {
            Score = 0;
            UIManager.Instance.SetScoreText($"Score : {Score}");
        }
    }
}


