﻿using System;
using Spaceship;
using UnityEngine;
using Utilities;
using Random = UnityEngine.Random;


namespace Manager
{
    public class GameManager : MonoSingleton<GameManager>
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;

        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private int enemySpaceshipHp;
        [SerializeField] private int enemySpaceshipMoveSpeed;
        
        private int enemyExplodeCount = 0;
        
        public event Action OnRestarted;
        private void Awake()
        {
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerSpaceship hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");
            Debug.Assert(enemySpaceshipHp > 0, "enemySpaceshipHp has to be more than zero");
            Debug.Assert(enemySpaceshipMoveSpeed > 0, "enemySpaceshipMoveSpeed has to be more than zero");

        }

        public void OnStartButtonClicked()
        {
            UIManager.Instance.StartDialog(false);
            UIManager.Instance.EndDialog(false);
            enemySpaceship.fireRate = 1f;
            StartGame();
        }

        private void StartGame()
        {
            ScoreManager.Instance.Init();
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship(1);
            UIManager.Instance.SetMaxHealth(playerSpaceshipHp);
            enemyExplodeCount = 0;
        }
        
        private void SpawnPlayerSpaceship()
        {
            var spaceship = Instantiate(playerSpaceship);
            spaceship.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Restart();
        }

        private void SpawnEnemySpaceship(int numberToSpawn)
        {
            for (int i = 0; i < numberToSpawn; i++)
            {
                var spaceship = Instantiate(enemySpaceship, new Vector3(Random.Range(-8f, 8f), enemySpaceship.transform.position.y), Quaternion.identity);
                spaceship.Init(enemySpaceshipHp, enemySpaceshipMoveSpeed);
                spaceship.OnExploded += OnEnemySpaceshipExploded;
            }
        }

        private void OnEnemySpaceshipExploded()
        {
            ScoreManager.Instance.SetScore(1);
            enemyExplodeCount++;
            SpawnEnemySpaceship(1);
            if (enemyExplodeCount % 2 == 0)
            {
                enemySpaceship.fireRate -= 0.2f;
                if (enemySpaceship.fireRate < 0.4f)
                {
                    enemySpaceship.fireRate = 0.4f;
                }
            }
        }

        private void Restart()
        {
            DestroyRemainingShips();
            UIManager.Instance.EndDialog(true);
            UIManager.Instance.SetFinalScoreText($"Your Total Score : {ScoreManager.Instance.Score}");
            OnRestarted?.Invoke();
        }

        private void DestroyRemainingShips()
        {
            var remainingEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in remainingEnemies)
            {
                Destroy(enemy);
            }
            
            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }

            var remainingItems = GameObject.FindGameObjectsWithTag("Items");
            foreach (var item in remainingItems)
            {
                Destroy(item);
            }
        }
    }
}
