﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Utilities;

namespace Manager
{
    public class UIManager : MonoSingleton<UIManager>
    {
        [SerializeField] private RectTransform startDialog;
        [SerializeField] private Button startButton;
        
        [SerializeField] private RectTransform endDialog;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;
        [SerializeField] private Button restartButton;
        
        [SerializeField] private Slider healthBar;
        
        void Start()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(startDialog != null, "dialog cannot be null");

            startButton.onClick.AddListener(GameManager.Instance.OnStartButtonClicked);
            restartButton.onClick.AddListener(GameManager.Instance.OnStartButtonClicked);
            
        }

        public void StartDialog(bool open)
        {
            if (open)
            {
                startDialog.gameObject.SetActive(true);
            }
            else
            {
                startDialog.gameObject.SetActive(false);
            }
        }

        public void SetScoreText(string text)
        {
            scoreText.text = text;
        }

        public void SetFinalScoreText(string text)
        {
            finalScoreText.text = text;
        }

        public void EndDialog(bool open)
        {
            if (open)
            {
                endDialog.gameObject.SetActive(true);
            }
            else
            {
                endDialog.gameObject.SetActive(false);
            }   
        }

        public void SetMaxHealth(int hp)
        {
            healthBar.maxValue = hp;
            healthBar.value = hp;
        }

        public void SetHealth(int hp)
        {
            healthBar.value = hp;
        }
    }
}
