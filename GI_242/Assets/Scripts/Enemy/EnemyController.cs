﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private Transform targetTransform;
        [SerializeField] private EnemySpaceship enemySpaceship;
       
        private void Start()
        {
            targetTransform = GameObject.FindGameObjectWithTag("Player").transform;
        }

        private void Update()
        {
            MoveToPlayer();
            
            enemySpaceship.Fire();
        }

         private void MoveToPlayer()
         {
             transform.position =
                 Vector2.MoveTowards(transform.position,
                     new Vector2(targetTransform.position.x, transform.position.y),
                     enemySpaceship.Speed * Time.deltaTime);

         }
    }    
}

