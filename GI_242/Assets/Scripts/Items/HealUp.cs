﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;

namespace Items
{
    public class HealUp : MonoBehaviour
    {
        [SerializeField] private int healAmount = 50;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                var player = other.gameObject.GetComponent<PlayerSpaceship>();
                if (player != null)
                {
                    player.GetHealUp(healAmount);
                }
                Destroy(gameObject);
            }
        }
    }
}
