﻿using System.Collections;
using System.Collections.Generic;
using Spaceship;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField] private Bullet powerBullet;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            var player = other.gameObject.GetComponent<PlayerSpaceship>();
            if (player != null)
            {
                player.GetPowerUpBullet(powerBullet);
            }
            Destroy(gameObject);
        }
    }
}
