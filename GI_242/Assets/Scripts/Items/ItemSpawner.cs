﻿using System;
using System.Collections;
using System.Collections.Generic;
using Items;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Items
{
    public class ItemSpawner : MonoBehaviour
    {
        [SerializeField] private HealUp healthPotion;
        [SerializeField] private PowerUp powerUp;

        private float time;
        private float time2;
        private float minTime = 10;
        private float maxTime = 30;
        private float spawnTime;
        private float spawnTime2 = 60;

        private void Awake()
        {
            Debug.Assert(healthPotion != null, "healthPotion cannot be null");

            SetRandomTime();
            time = 0;
            time2 = 0;
        }

        private void FixedUpdate()
        {
            time += Time.deltaTime;
            time2 += Time.deltaTime;

            if (time >= spawnTime)
            {
                Debug.Log("Spawned HealthPotion");
                SpawnHealthPotion();
                SetRandomTime();
                time = 0;
            }

            if (time2 >= spawnTime2)
            {
                SpawnPowerUp();
                time2 = 0;
            }
        }

        private void SpawnHealthPotion()
        {
            Instantiate(healthPotion, new Vector3(Random.Range(-8, 8), Random.Range(-3, 2), 0), Quaternion.identity);
        }

        private void SpawnPowerUp()
        {
            Instantiate(powerUp, new Vector3(Random.Range(-8, 8), Random.Range(-3, 2), 0), Quaternion.identity);
        }

        private void SetRandomTime()
        {
            spawnTime = Random.Range(minTime, maxTime);
            Debug.Log("Next object spawn in " + spawnTime + " seconds.");
        }
    }
}
